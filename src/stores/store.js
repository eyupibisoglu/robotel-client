import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({

	state:
	{
		accessToken: null
	},
	getters:
	{
		getAccessToken ( state )
		{
			return state.accessToken;
		}
	},
	mutations:
	{
		setAccessToken ( state, accessToken )
		{
			state.accessToken = accessToken
		}
	}
})